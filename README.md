# Description

Java courses test tasks by Bogacheva E.S. on 2019-11-03

- see doc/task.pdf for original task description
- see src/main/java/CourseTask/ServiceDefaultImplementation.java for Service implementation
- see src/test/java/CourseTask/TaskTest.java for JUnit tests


# Run

$ ./gradlew run

or

$ ./gradlew test
