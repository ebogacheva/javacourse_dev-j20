package CourseTask;

public interface Service {

    class BothZerosException extends IllegalArgumentException{

        public BothZerosException(){
            super();
        }
    }

    class IllegalDayException extends IllegalArgumentException{

        public IllegalDayException(String message){
            super(message);
        }
    }

    class IllegalMonthException extends IllegalArgumentException{

        public IllegalMonthException(String message){
            super(message);
        }
    }

    /**
     * Maximal common divisor (a.k.a. gcd)
     *
     * @param x - integral operand
     * @param y - integral operand
     * @return -  largest positive integer that divides each of the integers
     */
    int mcd (int x, int y);

    /**
     * Exclude repeating elements from the initial array.
     *
     * @param array - initial array
     * @return - all unique elements of the initial array
     */

    int[] unigue (int[] array);  //TODO: typo in method's name?

    /**
     * Calculates number of days from the beginning of the given year to the given date, not including the specified date.
     * Used astronomical year numbering (zero year exists).
     *
     * @param day - day of the specified date
     * @param month - month of the specified date
     * @param year - year of the specified date
     * @return - number of days from the 01 January of the specified year to the given date.
     */

    int days(int day, int month, int year);
}
