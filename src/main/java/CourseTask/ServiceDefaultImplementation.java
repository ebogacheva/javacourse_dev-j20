package CourseTask;

import java.util.HashSet;
import org.junit.runner.JUnitCore;
import org.junit.internal.TextListener;



public class ServiceDefaultImplementation implements Service {

    private final int[] daysFromYearStart = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
    private final int[] checkDaysInMonths = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public int mcd(int x, int y) {

            if (x == 0){
                if (y == 0) {
                    throw new BothZerosException();
                } else {
                    return y;
                }
            } else {
                if (y == 0) {
                    return x;
                }
            }

            if (x == Integer.MIN_VALUE && y == Integer.MIN_VALUE){
                throw new ArithmeticException("Maximal common divisor is out of integer type limits.");
            }

            long xL = Math.abs((long)x);
            long yL = Math.abs((long)y);

            while (xL != yL) {

                if (xL > yL) {
                    xL = xL - yL;
                } else {
                    yL = yL - xL;
                }
            }


            return (int)xL;
    }

    public int[] unigue(int[] array){

        if (array.length == 0) return new int[] {};

        final int capacity = (int) (array.length * 0.75);
        HashSet<Integer> inputHashSet = new HashSet<>(capacity);

        for (int i = 0; i < array.length; i++){
            inputHashSet.add(array[i]);
        }

        int[] result = new int[inputHashSet.size()];
        int j = 0;
        for (int i = 0; i < array.length; i++){
            if (inputHashSet.contains(array[i])){
                result[j] = array[i];
                inputHashSet.remove(array[i]);
                j++;
            }
        }

        return result;
    }



    public int days (int day, int month, int year){

        boolean leap;


        if (year % 4 != 0){
            leap = false;
        } else if (year % 100 != 0){
            leap = true;
        } else if (year % 400 != 0){
            leap = false;
        } else {
            leap = true;
        }

        if (month <= 0 || month > 12){
            throw new IllegalMonthException("Month should be in range from 1 to 12.");
        }

        if (day <= 0 || day > checkDaysInMonths[month - 1] || !leap && month == 2 && day > 28) {
            throw new IllegalDayException("Illegal number for day.");
        }

        int result = daysFromYearStart[month - 1] + day - 1;
        if (leap && month > 2){
            result++;
        }

        return result;

    }

    public void runningJUnitTest(){
        //see all tests in src/test/java/TaskTest class
        JUnitCore junit = new JUnitCore();
        junit.addListener(new TextListener(System.out));
        junit.run(TaskTest.class);
    }

}
